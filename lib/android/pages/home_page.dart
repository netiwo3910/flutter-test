import 'package:base_poc/android/pages/alarms_page.dart';
import 'package:base_poc/android/pages/microphone_page.dart';
import 'package:base_poc/android/pages/notifications_page.dart';
import 'package:base_poc/android/pages/share_page.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  final String title;

  HomePage({Key key, this.title}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;

  var _pages = [
    {"label": "Alarms", "icon": Icon(Icons.alarm), "page": AlarmsPage()},
    {"label": "Share", "icon": Icon(Icons.share), "page": SharePage()},
    {"label": "Notifications", "icon": Icon(Icons.view_list), "page": NotificationsPage()},
    {"label": "Microphone", "icon": Icon(Icons.mic), "page": MicrophonePage()},
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  List<BottomNavigationBarItem> _getNavigationItems() {

    var pages = <BottomNavigationBarItem>[];

    for (var page in _pages) {
      pages.add(
          BottomNavigationBarItem(
            icon: page['icon'],
            label: page['label'],
          )
      );
    }

    return pages;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text(widget.title),
      // ),
      body: _pages.elementAt(_selectedIndex)['page'],
      bottomNavigationBar: BottomNavigationBar(
        items: _getNavigationItems(),
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.teal,
        unselectedItemColor: Colors.grey,
        onTap: _onItemTapped,
      ),
    );
  }
}
