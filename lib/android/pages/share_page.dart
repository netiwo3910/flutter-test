import 'package:flutter/material.dart';
import 'package:share/share.dart';

class SharePage extends StatelessWidget {
  var _items = [
    {
      "title": "Card title 1",
      "subtitle": "Card subtitle 1",
      "image": "https://picsum.photos/id/512/500/300",
      "description":
          "Greyhound divisively hello coldly wonderfully marginally far upon excluding.",
    },
    {
      "title": "Card title 2",
      "subtitle": "Card subtitle 2",
      "image": "https://picsum.photos/id/870/500/300",
      "description":
          "Greyhound divisively hello coldly wonderfully marginally far upon excluding.",
    },
  ];

  List<Widget> _getCards() {
    var cards = <Widget>[];

    for (var item in _items) {
      cards.add(Card(
        clipBehavior: Clip.antiAlias,
        child: Column(
          children: [
            ListTile(
              leading: Icon(
                Icons.new_releases_outlined,
                size: 42.0,
                color: Colors.teal,
              ),
              title: Text(item['title']),
              subtitle: Text(
                item['subtitle'],
                style: TextStyle(color: Colors.black.withOpacity(0.6)),
              ),
            ),
            Image.network(item['image']),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                item['description'],
                style: TextStyle(color: Colors.black.withOpacity(0.6)),
              ),
            ),
            ButtonBar(
              alignment: MainAxisAlignment.end,
              children: [
                IconButton(
                    icon: Icon(Icons.share),
                    onPressed: () => _onShare(item['title']))
              ],
            ),
          ],
        ),
      ));
    }

    return cards;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Share'),
      ),
      body: ListView(
        children: _getCards(),
      ),
    );
  }

  _onShare(String title) async {
    await Share.share(title, subject: "Look at this movida!");
  }
}
