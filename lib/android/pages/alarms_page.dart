import 'package:flutter/material.dart';

class AlarmsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AlarmState();
}

class _AlarmState extends State<StatefulWidget> {
  static const _days = ["L", "M", "X", "J", "V", "S", "D"];

  var _alarms = [
    {
      "time": TimeOfDay(hour: 20, minute: 30),
      "days": ["J", "V"],
      "enabled": true
    },
    {
      "time": TimeOfDay(hour: 10, minute: 21),
      "days": ["L"],
      "enabled": true
    },
    {
      "time": TimeOfDay(hour: 08, minute: 30),
      "days": ["L", "M", "X", "J", "V"],
      "enabled": false
    },
    {"time": TimeOfDay(hour: 22, minute: 00), "days": [], "enabled": false},
  ];

  ListTile _getAlarmListTile(alarm) {
    var hour = alarm["time"].hour.toString().padLeft(2, '0');
    var minute = alarm["time"].minute.toString().padLeft(2, '0');

    var enabledStyle = TextStyle(color: Colors.teal);

    return ListTile(
      leading: Icon(Icons.alarm, size: 35, color: alarm["enabled"] ? Colors.teal : null),
      title: Text("$hour:$minute", style: TextStyle(fontSize: 20)),
      subtitle: Row(
          children: _days
              .map((e) => Container(
                    child: Text(e, style: alarm["days"].contains(e) ? enabledStyle : null),
                    padding: EdgeInsets.symmetric(horizontal: 5),
                  ))
              .toList()),
      trailing: IconButton(
        icon: Icon(Icons.delete),
        onPressed: () => setState(() => _alarms.remove(alarm)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Alarms'),
      ),
      body: ListView(
        children: _alarms.map(_getAlarmListTile).toList(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.alarm_add),
        onPressed: () async {
          TimeOfDay result = await showTimePicker(context: context, initialTime: TimeOfDay.now());
          if (result != null) {
            setState(() {
              var __days = _days.toList();
              __days.shuffle();
              _alarms.add(
                {"time": result, "days": __days.take(3), "enabled": false},
              );
            });
          }
        },
      ),
    );
  }
}
