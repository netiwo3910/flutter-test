import 'package:base_poc/ios/pages/home_page.dart';
import 'package:flutter/cupertino.dart';

class IOSApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      title: 'iOS Flutter Demo',
      theme: CupertinoThemeData(
        primaryColor: CupertinoColors.activeBlue,
      ),
      home: HomePage(title: 'iOS Flutter Demo Home Page'),
    );
  }
}