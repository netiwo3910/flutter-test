import 'dart:io' show Platform;

import 'package:base_poc/android/main.dart';
import 'package:base_poc/ios/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppService {
  static Widget getPlatformService() {
    if (Platform.isAndroid) {
      return AndroidApp();
    } else if (Platform.isIOS) {
      return IOSApp();
    }
  }
}
